﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace JiraBackendAPINETCORE.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FolderInfoController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<FolderInfoController> _logger;

        public FolderInfoController()
        {
         //_logger = logger2;
        }

        [HttpGet]
        public IEnumerable<Vorgaenge> Get()
        {
            var rng = new Random();
           return Enumerable.Range(1, 5).Select(index => new Vorgaenge { }
            /*{
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }*/
                )
            .ToArray();
        }
    }
}
