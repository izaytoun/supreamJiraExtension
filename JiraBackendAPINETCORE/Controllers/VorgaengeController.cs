﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JiraBackendAPINETCORE.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace JiraBackendAPINETCORE.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class vorgaengeController : ControllerBase
    {
    string vorgaengemockl = JsonConvert.SerializeObject(new
        {
            results = new List<Vorgaenge>()
    {
        new Vorgaenge {   Nummer = "SP-5001",
    Zusammenfassung= "Verantwortlichkeiten in WI zurückbauen FE",
    Status= "Geschlossen",
    Vorgangstyp= "Anforderung",
    Bearbeiter= "Team Entwicklungsleitung",
    Beobachter= "Alexander Grimm",
    Beobachter_Mailadressen= "grimm@solid-it.de",
    Aktualisiert= "2020-03-17T11=31=43.347Z",
    Testsysteme= "ATOS, ZBI",
    Frontend_Elemente= "U=\\supream\\entw.supream.local\\gemeinsam\\buero\\formular.asp\r\nU=\\supream\\entw.supream.local\\gemeinsam\\mietvertrag\\neu.asp\r\nU=\\supream\\entw.supream.local\\gemeinsam\\INCLUDE\\AltNeu\\sql_teilstring_verantwortliche.inc",
    Frontend_Elemente_vorbereitet= "true",
    Datenbank_Elemente= null,
    Sprint_Name_aktiv=null ,
    Version_Name=  ""},
       
    }
        });
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<vorgaengeController> _logger;

        public vorgaengeController(ILogger<vorgaengeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public SimpleResult Get()
        {
            
            return new SimpleResult()
            {
                Data = { }
            };
        }
    }


}
