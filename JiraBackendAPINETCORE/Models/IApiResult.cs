﻿using System;
using System.Collections.Generic;

namespace JiraBackendAPINETCORE.Models
{
    public interface IApiResult<T>
    {
        List<string> Errors { get; set; }

        T Data { get; set; }

       
    }
}