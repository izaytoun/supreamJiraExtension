﻿
using System.Collections.Generic;


namespace JiraBackendAPINETCORE.Models
{
    public class SimpleResult : IApiResult<object>
    {
        public List<string> Errors { get; set; }

        public object Data { get; set; }

    }
}
