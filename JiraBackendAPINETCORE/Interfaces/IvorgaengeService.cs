﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JiraBackendAPINETCORE.Interfaces
{
   
        

        public interface IvorgaengeService
        {
            IQueryable<Vorgaenge> GetAllZaehler(int userId, Guid token);
            IEnumerable<Vorgaenge> GetZaehler(int userId, Guid token, int tazId);
          
            List<string> GetZaehlerTypenBeschreibungen(int userId);
            List<string> GetZaehlerTypenMesseinheiten(int userId);
        }
    
}
