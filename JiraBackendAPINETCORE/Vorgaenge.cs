using System;

namespace JiraBackendAPINETCORE
{
    public class Vorgaenge
    {
        public String Nummer { get; set; }

        public String Zusammenfassung { get; set; }

        public String Status { get; set; }

        public String Vorgangstyp { get; set; }

        public String Bearbeiter { get; set; }
        public String Beobachter { get; set; }

        public String Beobachter_Mailadressen { get; set; }
        public String Aktualisiert { get; set; }
        public String Testsysteme { get; set; }

        public String Frontend_Elemente { get; set; }
        public String Frontend_Elemente_vorbereitet { get; set; }

        public String Datenbank_Elemente { get; set; }
        public String Sprint_Name_aktiv { get; set; }
        public String Version_Name { get; set; }
        public String Version_ReleaseDate { get; set; }



        public string[] SPelements { get; set; }



    }
}
