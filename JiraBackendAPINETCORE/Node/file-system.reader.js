"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var FileSystemReaderHelper = (function () {
    function FileSystemReaderHelper(directory) {
        this.directory = directory;
        this.directory2 = 'S:/';
    }
    FileSystemReaderHelper.prototype.list = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve, reject) {
                        fs.readdir(_this.directory, _this.directory2, function (error, filenames) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve(filenames);
                            }
                        });
                    })];
            });
        });
    };
    FileSystemReaderHelper.prototype.read = function (path) {
        return __awaiter(this, void 0, void 0, function () {
            var fileencoded, pahtSplitted_1;
            return __generator(this, function (_a) {
                try {
                    console.log('FileSystemReaderHelper -> constructor -> path', path);
                    fileencoded = path;
                    pahtSplitted_1 = fileencoded.split('\n');
                    return [2, new Promise(function (resolve, reject) {
                            pahtSplitted_1.forEach(function (pathsplitted) {
                                var pathsplittedEncoded = pathsplitted;
                                console.log('1727002042020 FileSystemReaderHelper -> constructor -> splitted', pathsplittedEncoded);
                                console.log('1924 FilesHelper -> readFile -> fileencoded match 1', pathsplittedEncoded);
                                pathsplittedEncoded = pathsplittedEncoded.replace('\r', '');
                                fs.readFile("" + pathsplittedEncoded, function (error, data) {
                                    if (error) {
                                        console.log('error', error);
                                        reject(error);
                                    }
                                    else {
                                        resolve(data.toString());
                                    }
                                });
                            });
                        })];
                }
                catch (error) {
                    console.log('FileSystemReaderHelper -> constructor -> error', error);
                }
                return [2];
            });
        });
    };
    FileSystemReaderHelper.prototype.readState = function (filename) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    return [2, new Promise(function (resolve, reject) {
                            fs.stat("" + filename, function (error, stat) {
                                if (error) {
                                    console.log('error', error);
                                    reject(error);
                                }
                                else {
                                    resolve(stat);
                                }
                            });
                        })];
                }
                catch (error) {
                    console.log('FileSystemReaderHelper -> constructor -> error', error);
                }
                return [2];
            });
        });
    };
    FileSystemReaderHelper.prototype.readFiles = function (dirname, onFileContent, onError) {
        fs.readdir(dirname, function (err, filenames) {
            if (err) {
                onError(err);
                return;
            }
            filenames.forEach(function (filename) {
                fs.readFile(dirname + filename, 'utf-8', function (err, content) {
                    if (err) {
                        onError(err);
                        return;
                    }
                    return onFileContent(filename, content);
                });
            });
        });
    };
    FileSystemReaderHelper.prototype.readAnyOf = function (filenames) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, filenames_1, file, err_1, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 5, , 9]);
                        _i = 0, filenames_1 = filenames;
                        _b.label = 1;
                    case 1:
                        if (!(_i < filenames_1.length)) return [3, 4];
                        file = filenames_1[_i];
                        return [4, this.read(file)];
                    case 2: return [2, _b.sent()];
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4: return [3, 9];
                    case 5:
                        err_1 = _b.sent();
                        if (!(filenames.length > 0)) return [3, 7];
                        return [4, this.readAnyOf(filenames.slice(1, filenames.length))];
                    case 6:
                        _a = _b.sent();
                        return [3, 8];
                    case 7:
                        _a = undefined;
                        _b.label = 8;
                    case 8: return [2, _a];
                    case 9: return [2];
                }
            });
        });
    };
    return FileSystemReaderHelper;
}());
exports.FileSystemReaderHelper = FileSystemReaderHelper;
//# sourceMappingURL=file-system.reader.js.map