"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var readers_1 = require("@nestjs/cli/lib/readers");
var file_system_reader_1 = require("./file-system.reader");
var FilesHelper = (function () {
    function FilesHelper() {
        this.splittCharacter = '\\';
    }
    FilesHelper.prototype.findFrontendElement = function (frontenElementPath, testystem) {
        return __awaiter(this, void 0, void 0, function () {
            var fileReader;
            return __generator(this, function (_a) {
                try {
                    fileReader = new readers_1.FileSystemReader(frontenElementPath);
                    return [2, fileReader.read(frontenElementPath).then(function (value) {
                            return __awaiter(this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    return [2, value.toString()];
                                });
                            });
                        }, function (value) {
                            return __awaiter(this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    return [2, value.toString()];
                                });
                            });
                        })];
                }
                catch (error) { }
                return [2];
            });
        });
    };
    FilesHelper.prototype.findMatchingPart = function (frontenElementPath) {
        try {
            var frontenElementPathArray;
            var tempelement;
            var frontenElementPathmatchingPart;
            var testystemPath;
            frontenElementPathArray = frontenElementPath.split('\\');
            console.log('13 FilesHelper -> findMatchingPart -> frontenElementPathArray', frontenElementPathArray);
            frontenElementPathArray.forEach(function (element) {
                if (!frontenElementPathmatchingPart) {
                    tempelement = element;
                    console.log('14 FilesHelper -> findMatchingPart -> tempelement', tempelement);
                    frontenElementPathmatchingPart = tempelement.match('entw.supream.local');
                    console.log('15 FilesHelper -> findMatchingPart -> frontenElementPathmatchingPart', frontenElementPathmatchingPart);
                }
            });
            return frontenElementPathmatchingPart;
        }
        catch (error) {
            console.log('FilesHelper -> findMatchingPart -> error', error);
        }
    };
    FilesHelper.prototype.testSystemPart = function (frontenElementPath, testystem, testsystemMatchingFile) {
        try {
            var testFiles;
            var testFile;
            var testPath;
            var testsystems = testystem.split(',');
            console.log(' 20 FilesHelper -> findFrontendElement -> testsystems', testsystems);
            console.log('FilesHelper -> findFrontendElement -> testystem', testystem);
            var frontendElementRootPath = frontenElementPath.split(testsystemMatchingFile + this.splittCharacter);
            console.log(' 21 FilesHelper -> findFrontendElement -> frontendElementRootPath', frontendElementRootPath[0]);
            testFiles = this.getFolders(frontendElementRootPath[0]);
            console.log('22 FilesHelper -> findFrontendElement -> testFiles', testFiles);
            testsystems.forEach(function (testSystem) {
                testFiles.forEach(function (element) {
                    if (element.includes('-' + testSystem.toLowerCase().replace(' ', '') + '.')) {
                        testFile = element;
                        console.log('!!!!!!!!!!!!!!!FilesHelper -> findFrontendElement -> testFile', testFile);
                    }
                });
            });
            testPath = frontenElementPath.replace(testsystemMatchingFile, testFile);
            return testPath;
        }
        catch (error) {
            console.log(error);
        }
    };
    FilesHelper.prototype.getFolders = function (dir) {
        try {
            console.log('FilesHelper -> getFolders -> dir', dir);
            var list = fs.readdirSync(dir);
            console.log(' 24 FilesHelper -> getFolders -> list', list);
            return list;
        }
        catch (error) {
            console.log('25 FilesHelper -> getFolders -> error', error);
        }
    };
    FilesHelper.prototype.readFile = function (directoryEntwicklung, directoryTest) {
        return __awaiter(this, void 0, void 0, function () {
            var fileSystemReaderEntwicklung, fileSystemReaderTest, fileEntwicklung, fileTest, fileTestState, fileEntwState, promises, promise1, promise2, promise3, promise4;
            return __generator(this, function (_a) {
                try {
                    fileSystemReaderEntwicklung = new file_system_reader_1.FileSystemReaderHelper(directoryEntwicklung);
                    fileSystemReaderTest = new file_system_reader_1.FileSystemReaderHelper(directoryTest);
                    promises = [];
                    promise1 = fileSystemReaderTest.read(directoryTest);
                    try {
                        promise1.then(function (value) {
                            fileEntwicklung = value;
                            console.log('FilesHelper -> readFile -> fileEntwicklung', fileEntwicklung);
                        }, function (err) {
                        });
                    }
                    catch (error) {
                        console.log('FilesHelper -> readFile ->promise1 --> error', error);
                    }
                    promise2 = fileSystemReaderEntwicklung.read(directoryEntwicklung);
                    try {
                        promise2.then(function (value) {
                            fileTest = value;
                        }, function (err) {
                        });
                    }
                    catch (error) { }
                    promise3 = fileSystemReaderEntwicklung.readState(directoryEntwicklung);
                    promise3.then(function (value) {
                        fileTestState = value;
                    }, function (err) {
                    });
                    promise4 = fileSystemReaderEntwicklung.readState(directoryTest);
                    promise4.then(function (value) {
                        fileEntwState = value;
                    }, function (err) {
                    });
                    promises.push(promise1, promise2, promise3, promise4);
                    return [2, Promise.all(promises).then(function () {
                            return {
                                FileEntwicklung: fileEntwicklung,
                                FileEntwState: fileEntwState,
                                FileTest: fileTest,
                                FileTestState: fileTestState,
                            };
                        })];
                }
                catch (error) {
                    console.log('FilesHelper -> readFile -> error', error);
                }
                return [2];
            });
        });
    };
    FilesHelper.prototype.readContentFromFile = function (fileContent) {
        var re = /[sS]{1,1}[pP]{1,1}-[0-9]{1,10}/g;
        return fileContent.match(re).toString();
    };
    FilesHelper.prototype.readSPelementsFromFile = function (file, testSystem, character) {
        return __awaiter(this, void 0, void 0, function () {
            var SPelements_1, files, SPelementsForEntwicklung, SPelementsForTest, promises, promiseEntwicklungFile, promiseTestFile, fileEntwState, promise5;
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    SPelements_1 = [];
                    promises = [];
                    files = file.split(character);
                    try {
                        files.forEach(function (file) {
                            var fileSystemReaderEntwicklung = new file_system_reader_1.FileSystemReaderHelper(file);
                            var promise4 = fileSystemReaderEntwicklung.readState(file);
                            promise4.then(function (value) {
                                fileEntwState = value;
                                SPelements_1.push({
                                    EntwicklungState: fileEntwState,
                                    EntwicklungfileState: file,
                                });
                            }, function (err) {
                            });
                        });
                    }
                    catch (error) { }
                    files.forEach(function (file) { return __awaiter(_this, void 0, void 0, function () {
                        var fileencoded, fileSystemReader, matchingPart, testSystemPart, fileSystemReaderTest;
                        var _this = this;
                        return __generator(this, function (_a) {
                            fileencoded = file;
                            if (fileencoded.match(/%\\r$/)) {
                                fileencoded = fileencoded.substring(0, fileencoded.length - 3);
                            }
                            if (fileencoded.match(/%\\n$/)) {
                                fileencoded = fileencoded.substring(0, fileencoded.length - 3);
                            }
                            if (fileencoded.match(/%0D$/)) {
                            }
                            console.log('FilesHelper -> readFile -> fileencoded', fileencoded);
                            fileSystemReader = new file_system_reader_1.FileSystemReaderHelper(fileencoded);
                            promiseEntwicklungFile = fileSystemReader.read(fileencoded);
                            promiseEntwicklungFile.then(function (value) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4, this.readContentFromFile(value)];
                                        case 1:
                                            SPelementsForEntwicklung = _a.sent();
                                            SPelements_1.push({
                                                SPelementsForEntwicklung: SPelementsForEntwicklung,
                                                File: fileencoded,
                                                TestSystem: testSystemPart,
                                            });
                                            return [2];
                                    }
                                });
                            }); }, function (err) {
                            });
                            matchingPart = this.findMatchingPart(file);
                            testSystemPart = this.testSystemPart(file, testSystem, matchingPart);
                            promiseTestFile = fileSystemReader.read(testSystemPart);
                            try {
                                promiseTestFile.then(function (value) { return __awaiter(_this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4, this.readContentFromFile(value)];
                                            case 1:
                                                SPelementsForTest = _a.sent();
                                                SPelements_1.push({
                                                    SPelementsForTest: SPelementsForTest,
                                                    File: testSystemPart,
                                                    EntwicklungMatchingParth: file,
                                                });
                                                return [2];
                                        }
                                    });
                                }); }, function (err) { });
                            }
                            catch (error) { }
                            try {
                                fileSystemReaderTest = new file_system_reader_1.FileSystemReaderHelper(testSystemPart);
                                promise5 = fileSystemReaderTest.readState(testSystemPart);
                                promise5.then(function (value) {
                                    fileEntwState = value;
                                    SPelements_1.push({
                                        TestState: fileEntwState,
                                        TestfileState: file,
                                    });
                                }, function (err) {
                                });
                            }
                            catch (error) { }
                            return [2];
                        });
                    }); });
                    promises.push(promiseEntwicklungFile, promiseTestFile, promise5);
                    return [2, Promise.all(promises).then(function () {
                            return {
                                SPelements: SPelements_1,
                            };
                        })];
                }
                catch (error) { }
                return [2];
            });
        });
    };
    return FilesHelper;
}());
exports.FilesHelper = FilesHelper;
//# sourceMappingURL=comparefile.js.map